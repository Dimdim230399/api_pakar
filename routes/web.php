<?php


$router->get('/', function () use ($router) {
    return $router->app->version();
});
// login
$router->post("/register", "AuthController@register");
$router->post("/login", "AuthController@login");
$router->post("/logout", "AuthController@logout");

$router->group(['namespace' => 'master'], function () use ($router) {
// $router->group(['middleware' => 'auth', 'namespace' => 'master'], function () use ($router) {
    
    #Gejala
    $router->get("/getGejala", "gejalaController@getGejala");
    $router->post("/postGejala", "gejalaController@postGejala");    
    $router->post("/postDGejala", "gejalaController@postDGejala");

    #Penyakit
    $router->get("/getPenyakit", "penyakitController@getPenyakit");
    $router->post("/postPenyakit", "penyakitController@postPenyakit");    
    $router->post("/postDPenyakit", "penyakitController@postDPenyakit");    

    #Mapping
    $router->get("/getMapping", "mappingController@getMapping");
    $router->post("/postMapping", "mappingController@postMapping");    
    $router->post("/postDMapping", "mappingController@postDMapping");           

    #Pasien
    $router->get("/getPasien/{id}", "pasienController@getPasien");
    $router->post("/postPasien", "pasienController@postPasien");
    $router->post("/postUPasien", "pasienController@postUPasien");
    $router->post("/postDPasien", "pasienController@postDPasien");    

    #History
    $router->get("/getHistory/{id}", "historyController@getHistory");
    $router->post("/postHistory", "historyController@postHistory");
    $router->post("/postDHistory", "historyController@postDHistory");

    #History Gejala
    $router->get("/getHistoryG/{id}", "historyGController@getHistoryG");
    $router->post("/postHistoryG", "historyGController@postHistoryG");
    $router->post("/postDHistoryG", "historyGController@postDHistoryG");

    #History Penyakit
    $router->get("/getHistoryP/{id}", "historyPController@getHistoryP");
    $router->post("/postDHistoryP", "historyPController@postDHistoryP");
}); 
    
    #Soal
    $router->get("/getSoal", "soalController@getSoal");
    $router->post("/postDataDiri", "soalController@postDataDiri");
    $router->post("/postJawaban", "soalController@postJawaban");
    $router->get("/getHasil/{id_history}", "soalController@getHasil"); 


