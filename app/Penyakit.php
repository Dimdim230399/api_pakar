<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Penyakit extends Model 
{
    protected $table = 'm_penyakit';
    protected $primaryKey ='id';
  
    protected $fillable = [
      'nama_penyakit', 'created_by', 'updated_by'
    ];

}
