<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Gejala extends Model 
{
    protected $table = 'm_gejala';
    protected $primaryKey ='id';
  
    protected $fillable = [
      'nama_gejala', 'created_by', 'updated_by'
    ];

}
