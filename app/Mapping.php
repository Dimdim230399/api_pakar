<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Mapping extends Model 
{
    protected $table = 'mapping_gejala';
    protected $primaryKey ='id';

    protected $fillable = [
        'id_penyakit', 'id_gejala', 'created_by', 'updated_by'
    ];

}
