<?php

namespace App\Http\Controllers;

Use DB;
use Redirect;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Validator;
use App\helper\api_helper;
use App\Pasien;

class soalController extends Controller
{
    function getSoal()
    {
        $query = DB::table("m_gejala as h")
            ->orderBy('id', 'ASC')
            ->get();   
            $data=[];           
        foreach ($query as $key => $value) {
            $data[$key]['id']=$value->id;
            $data[$key]['nama_gejala']=$value->nama_gejala;
        }   
        return generateJson($data);                
    } 


    function postDataDiri(Request $request)
    {                
        $insert = array(     
            'hp' => $request->hp,            
            'nama' => $request->nama,            
            'usia' => $request->usia,         
            'tinggi_badan' => $request->tinggi_badan,       
            'berat_badan' => $request->berat_badan       
        );                    

        $pasien = new Pasien;
        $pasien->nama = $request->nama;
        $pasien->hp = $request->hp; 
        $pasien->usia = $request->usia; 
        $pasien->tinggi_badan = $request->tinggi_badan;
        $pasien->berat_badan = $request->berat_badan;
        $pasien->save();

        if ( $pasien->save() == true ){
            $data['code']="100";
            $data['message']="Sukses tambah data pasien!";
        } else {
            $data['code']="404";
            $data['message']="Gagal tambah data pasien!";
            return response()->json($data);
        }               
       

        $dataPasien = Pasien::where('hp', $request->hp)->latest('created_at')->first();

        $insert = array(         
            'id_pasien' => $dataPasien['id'], 
            'created_at' => date("Y-m-d H:i:s")
        );                    

        $query = DB::table('history')->insert($insert); 

        $dataHistory = DB::table('history as h')
                        ->selectRaw('h.*, p.nama')
                        ->leftJoin('pasien as p', 'h.id_pasien', '=', 'p.id')
                        ->where('id_pasien', $dataPasien['id'])->latest('created_at')->first();
        return response()->json($dataHistory);
    }

    function postJawaban(Request $request)
    {                
        if ($request->kode_gejala == '' && $request->id_history=='') {

        } else {
                $insert = array(         
                    'kode_gejala' => $request->kode_gejala, 
                    'id_history' => $request->id_history            
                );                    
                $query = DB::table('history_gejala')->insert($insert); 
                if ( $query == true ){
                    $data['code']="100";
                    $data['message']="Sukses tambah data history gejala dan penyakit!";
                } else {
                    $data['code']="404";
                    $data['message']="Gagal tambah data history gejala dan penyakit!";
                }         
        }
    }    

    public function getHasil($id_history)
    {
    $hasil = DB::select("
    SELECT RAW.*, nama_penyakit, deskripsi_penyakit from 
        (      
         SELECT hasil.id_penyakit,ceil(jumlah_kemungkinan::decimal/count_kp::decimal*100) as presentase 
        from 
        (
            select a.id_penyakit, count(a.id_penyakit) as jumlah_kemungkinan
                from 
                (
                    select hg.*, id_penyakit 
                    from history_gejala hg, mapping_gejala pgp
                    where hg.kode_gejala = pgp.id_gejala
                    and id_history=?                                                                  
                )a
                group by a.id_penyakit
                order by jumlah_kemungkinan desc
                limit 3
         )hasil,
            (
                select id_penyakit,COUNT(id_penyakit) as count_kp from mapping_gejala mgp
                group by id_penyakit
            )raw
          where hasil.id_penyakit = raw.id_penyakit
     )RAW,m_penyakit mp
      WHERE RAW.id_penyakit = mp.id
      ORDER BY presentase DESC          
     ",[$id_history]);
        return $hasil;
    }

    public function getPerawatan($id_penyakit)
    {
        $hasil = DB::select('select deskripsi_perawatan as dp from mapping_gejala_penyakit where kode_penyakit = ?', [$id_penyakit]);
        return $hasil;
    }

}

