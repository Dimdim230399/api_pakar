<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use DB;
 
class AuthController extends Controller
{
    public function register(Request $request)
    {
        $this->validate($request, [
            'email' => 'required|unique:users|max:255',
            'password' => 'required|min:6',
            'username' => 'required',
            'name' => 'required',
            'level' => 'required'
        ]);

        $email = $request->input("email");
        $username = $request->input("username");
        $password = $request->input("password");
        $level = $request->input("level");
        $name = $request->input("name");

        $hashPwd = Hash::make($password);

        $data = [
            "email" => $email,
            "password" => $hashPwd,
            "username" => $username,
            "level" => $level,
            "name" => $name,
        ];
 
        if (User::create($data)) {
            $out = [
                "message" => "register_success",
                "code"    => 201,
            ];
        } else {
            $out = [
                "message" => "failed_regiser",
                "code"   => 404,
            ];
        }
 
        return response()->json($out, $out['code']);
    }
 
    public function login(Request $request)
    {
        
        $this->validate($request, [
            'username' => 'required',
            'password' => 'required|min:6'
        ]);
            
        $username = $request->input("username");
        $password = $request->input("password");
        $user = User::where("username", $username)->first();
        // dd($user);
        // return response()->json($user->password);
        
        
        if (!$user) {
            $out = [
                "message" => "login_failed",
                "code"    => 401,
                "result"  => [
                    "token" => null,
                ]
            ];
            return response()->json($out, $out['code']);
        }

        // return response()->json([$password,Hash::check($password, '$2y$10$G1LiYc51Gf3R0HT5s6ZyfOr0G18DfePULuE/H9BJRsxF3gZteCe9a'),Hash::make($password),' = ',$user->password]);
        // return response()->json([$password,Hash::make($password),' = ',$user->password]);
        if (Hash::check($password, $user->password)) {
            // return 'ok';
            if ($user->level == 'admin') {
                $newtoken  = 'adm'.$this->generateRandomString();
            } elseif ($user->level == 'user') {
                $newtoken  = 'usr'.$this->generateRandomString();
            }
 
            $user->update([
                'token' => $newtoken
            ]);
 
            $out = [
                "message" => "login_success",
                "code"    => 200,
                "result"  => [
                    "id" => $user->id,
                    "username" => $user->username,
                    "password" => $user->password,
                    "name" => $user->name,
                    "password" => $user->password,
                    "email" => $user->email,
                    "level" => $user->level,
                    "token" => $newtoken,
                ]
            ];
        } else {
            $out = [
                "message" => "login_failed",
                "code"    => 401,
                "result"  => [
                    "token" => null,
                ]
            ];
        }
 
        return response()->json($out, $out['code']);
    }
 
    function generateRandomString($length = 80)
    {
        $karakkter = '012345678dssd9abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $panjang_karakter = strlen($karakkter);
        $str = '';
        for ($i = 0; $i < $length; $i++) {
            $str .= $karakkter[rand(0, $panjang_karakter - 1)];
        }
        return $str;
    }

    function logout(Request $request)
    {
        $this->validate($request, [
            'id' => 'required',
            // 'username' => 'required',
            // 'password' => 'required|min:5'
        ]);

        $id = $request->input("id");
        // $username = $request->input("username");
        // $password = $request->input("password");
        $user = db::table('users')->where("id", $id)->first();
        // dd($user);
        $ara = array( 'token' => '');
        if ($user) {
            DB::table('users')->where("id", $id)->update($ara);
            $out = [
                "message" => "logout berhasil",
                "code"    => 201,
                "result"  => [
                    "token" => '',
                ]
            ];
        }else {
            $out = [
                "message" => "logout gagal",
                "code"    => 401,
                "result"  => [
                    "token" => null,
                ]
            ];
        }
        return response()->json($out, $out['code']);
    }
}
