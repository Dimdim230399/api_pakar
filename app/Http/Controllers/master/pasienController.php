<?php

namespace App\Http\Controllers\master;

Use DB;
use Redirect;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Validator;
use App\Pasien;

class pasienController extends Controller
{
    function getPasien($id)
    {

        if($id == 'all'){                    

            $query = DB::table("pasien")                
                ->orderBy('nama', 'ASC')
                ->get();
            
            $data = array();

            foreach ($query as $key => $value) {
                $data[$key]['id']=$value->id;
                $data[$key]['hp']=$value->hp;
                $data[$key]['nama']=$value->nama;
                $data[$key]['usia']=$value->usia.' th';
                $data[$key]['tinggi_badan']= $value->tinggi_badan.' cm';
                $data[$key]['berat_badan']=$value->berat_badan. ' kg';
            }   

            return response()->json($data);

        } else {

            $query = DB::table("pasien")       
                ->where('id', $id)                         
                ->get();                        

            return response()->json($query);

        }                
    }

    function postPasien(Request $request)
    {                
        $insert = array(               
            'hp' => $request->hp,            
            'nama' => $request->nama,            
            'usia' => $request->usia,         
            'tinggi_badan' => $request->tinggi_badan,       
            'berat_badan' => $request->berat_badan       
        );                    

        $pasien = new Pasien;
        $pasien->nama = $request->nama;
        $pasien->hp = $request->hp; 
        $pasien->usia = $request->usia; 
        $pasien->tinggi_badan = $request->tinggi_badan;
        $pasien->berat_badan = $request->berat_badan;
        $pasien->save();

        if ( $pasien->save() == true ){
            $data['code']="100";
            $data['message']="Sukses tambah data pasien!";
        } else {
            $data['code']="404";
            $data['message']="Gagal tambah data pasien!";
            return response()->json($data);
        }                

        $datas = Pasien::firstWhere('hp', $request->hp);
        return response()->json($datas['id']);
    }


    public function postUPasien(Request $request)
    {        

        $update = DB::table('pasien')->where([
            ['id', '=', $request->id]            
        ])->update([   
            'hp' => '088000',            
            // 'hp' => $request->hp,            
            'nama' => $request->nama,            
            'usia' => 20,            
            // 'usia' => $request->usia,            
            'tinggi_badan' => 170,            
            // 'tinggi_badan' => $request->tinggi_badan,            
            'berat_badan' => 68       
            // 'berat_badan' => $request->berat_badan                                    
        ]);   
        
        if ( $update == true ){
            $data['code']="100";
            $data['message']="Sukses ubah data pasien!";
        } else {
            $data['code']="404";
            $data['message']="Gagal ubah data pasien!";
        }                   

        return response()->json($data);
    }  

    
    function postDPasien(Request $request)
    {
    
        $query = DB::table('pasien')
            ->where('id', $request['id'])->delete();  

        if ( $query == true ) {
            $data['code']="100";
            $data['message']="Sukses hapus data pasien!";
        } else {
            $data['code']="404";
            $data['message']="Gagal hapus data pasien!";
        }        

        return response()->json($data);
    }
}

