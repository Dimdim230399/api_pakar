<?php

namespace App\Http\Controllers\master;

Use DB;
use Redirect;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Validator;

class perawatanController extends Controller
{
    function getPerawatan($id)
    {

        if($id == 'all'){                                

            $query = DB::table("m_perawatan as r")
                ->selectRaw('r.*, p.nama_penyakit as nm_penyakit')
                ->leftJoin('m_penyakit as p', 'r.id_penyakit', '=', 'p.id')
                ->orderBy('nama_penyakit', 'ASC')
                ->get();            
            
            $data = array();

            foreach ($query as $key => $value) {
                $data[$key]['id']=$value->id;
                $data[$key]['id_penyakit']=$value->id_penyakit;
                $data[$key]['nm_penyakit']=$value->nama_p;
                $data[$key]['deskripsi_perawatan']=$value->deskripsi_perawatan;                
                $data[$key]['created_at']=$value->created_at;
                $data[$key]['created_by']=$value->created_by;
                $data[$key]['updated_at']=$value->updated_at;                
                $data[$key]['updated_by']=$value->updated_by;
            }   

            return generateJson($data);

        } else {

            $query = DB::table("perawatan")       
                ->where('id', $id)                         
                ->get();                        

            return response()->json([
                'id' => 'id',
                'data' => $query
            ]);

        }                
    }

    function postPerawatan(Request $request)
    {        

        #Sementara
        $admin = "admin";
        $id_penyakit = $request->id_penyakit;

        #Awal kegabutan
        $disease = DB::table("penyakit")
            ->select('nama_penyakit')
            ->where('id', $id_penyakit)->get();

        $namaPenyakit = $disease[0]->nama_penyakit;
        #Akhir dari kegabutan

        $insert = array(
            'id_penyakit' => $id_penyakit,            
            'deskripsi_perawatan' => "Query request insert deskripsi perawatan penyakit $namaPenyakit disini!",
            // 'deskripsi_perawatan' => $request->deskripsi_perawatan,
            'created_at' => date("Y-m-d H:i:s"), 
            'created_by' => $admin,
            'updated_at' => $request->updated_at,            
            'updated_by' => $request->updated_by
        );         

        $query = DB::table('perawatan')->insert($insert); 

        if ( $query == true ){
            $data['code']="100";
            $data['message']="Sukses tambah data perawatan!";
        } else {
            $data['code']="404";
            $data['message']="Gagal tambah data perawatan!";
        }                

        return response()->json($data);
    }

    public function postUPerawatan(Request $request)
    {
        #Sementara
        $admin = "admin";
        $id_penyakit = $request->id_penyakit;

        #Awal kegabutan
        $disease = DB::table("penyakit")
            ->select('nama_penyakit')
            ->where('id', $id_penyakit)->get();

        $namaPenyakit = $disease[0]->nama_penyakit;
        #Akhir dari kegabutan

        $update = DB::table('perawatan')->where([
            ['id', '=', $request->id]            
        ])->update([              
            'id_penyakit' => $id_penyakit,                
            'deskripsi_perawatan' => "Query request update deskripsi perawatan penyakit $namaPenyakit disini!", 
            // 'deskripsi_perawatan' => $request->deskripsi_perawatan,                                          
            'updated_at' => date("Y-m-d H:i:s"),
            'updated_by' => $admin
        ]);   
        
        if ( $update == true ){
            $data['code']="100";
            $data['message']="Sukses ubah data perawatan!";
        } else {
            $data['code']="404";
            $data['message']="Gagal ubah data perawatan!";
        }                   

        return response()->json($data);
    }  

    function postDPerawatan(Request $request)
    {
    
        $query = DB::table('perawatan')
            ->where('id', $request['id'])->delete();  

        if ( $query == true ) {
            $data['code']="100";
            $data['message']="Sukses hapus data perawatan!";
        } else {
            $data['code']="404";
            $data['message']="Gagal hapus data perawatan!";
        }        

        return response()->json($data);
    }
}

