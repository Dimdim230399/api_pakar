<?php

namespace App\Http\Controllers\master;

Use DB;
use Redirect;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Validator;

class historyGController extends Controller
{      

    function getHistoryG($id)
    {

        if($id == 'all'){                    

            $query = DB::table("history_gejala as hg")
                ->selectRaw('hg.*, g.nama_gejala as nama_g')
                ->leftJoin('m_gejala as g', 'hg.kode_gejala', '=', 'g.id')
                ->leftJoin('history as h', 'hg.id_history', '=', 'h.id')                
                ->orderBy('id', 'ASC')
                ->get();              
            
            $data = array();

            foreach ($query as $key => $value) {
                $data[$key]['id']=$value->id;
                $data[$key]['kode_gejala']=$value->kode_gejala;
                $data[$key]['nama_g']=$value->nama_g;
                $data[$key]['id_history']=$value->id_history;                                
            }   

            return response()->json($data);

        } else {

            $query = DB::table("history_gejala")       
                ->where('id', $id)                         
                ->get();                        

            return response()->json($query);

        }                
    }     

  

    function postDHistoryG(Request $request)
    {
    
        $query = DB::table('history_gejala')
            ->where('id', $request['id'])->delete();  

        if ( $query == true ) {
            $data['code']="100";
            $data['message']="Sukses hapus data history gejala!";
        } else {
            $data['code']="404";
            $data['message']="Gagal hapus data history gejala!";
        }        

        return response()->json($data);
    }
}

