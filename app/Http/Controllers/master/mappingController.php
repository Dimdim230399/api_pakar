<?php

namespace App\Http\Controllers\master;

Use DB;
use Redirect;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Validator;
use App\Mapping;

class mappingController extends Controller
{
    function getMapping()
    {                                    
        $query = DB::table("mapping_gejala as m")
            ->selectRaw('m.*, g.nama_gejala as nama_g, p.nama_penyakit as nama_p')
            ->leftJoin('m_gejala as g', 'm.id_gejala', '=', 'g.id')
            ->leftJoin('m_penyakit as p', 'm.id_penyakit', '=', 'p.id')                
            ->orderBy('nama_g', 'ASC')
            ->get();            
        
        $data = array();
        foreach ($query as $key => $value) {
            $data[$key]['id']=$value->id;                
            $data[$key]['id_penyakit']=$value->id_penyakit;
            $data[$key]['nama_p']=$value->nama_p;
            $data[$key]['id_gejala']=$value->id_gejala;
            $data[$key]['nama_g']=$value->nama_g;                         
            $data[$key]['created_by']=$value->created_by;
            $data[$key]['updated_by']=$value->updated_by;
            $data[$key]['created_at']=$value->created_at;
            $data[$key]['updated_at']=$value->updated_at;                
        }   
        
        return generateJson($data);         
    }

    function postMapping(Request $request)    
    {        
        $insert = [
            'id_penyakit' => $request->id_penyakit,                        
            'id_gejala' => $request->id_gejala,                                          
            'created_by' => $request->created_by,                      
            'updated_by' => $request->updated_by
        ];         

        $query = Mapping::updateOrCreate(['id' => $request->id], $insert);             

        if ( $query == true ){
            $data['code']="100";
            $data['message']="Sukses tambah/ubah data mapping!";
        } else {
            $data['code']="404";
            $data['message']="Gagal tambah/ubah data mapping!";
        }                

        return generateJson($data); 
    }    

    function postDMapping(Request $request)
    {
    
        $query = DB::table('mapping_gejala')
            ->where('id', $request['id'])->delete();  

        if ( $query == true ) {
            $data['code']="100";
            $data['message']="Sukses hapus data mapping!";
        } else {
            $data['code']="404";
            $data['message']="Gagal hapus data mapping!";
        }        

        return generateJson($data); 
    }
}

