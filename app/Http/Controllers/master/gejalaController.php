<?php

namespace App\Http\Controllers\master;

Use DB;
use Redirect;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Validator;
use App\Gejala;

class gejalaController extends Controller
{
    
    function getGejala()
    {                   
        $query = DB::table("m_gejala")                
            ->orderBy('nama_gejala', 'ASC')
            ->get();

        $data = array();

        foreach ($query as $key => $value) {
            $data[$key]['id']=$value->id;
            $data[$key]['nama_gejala']=$value->nama_gejala;
            $data[$key]['created_by']=$value->created_by;
            $data[$key]['updated_by']=$value->updated_by;
            $data[$key]['created_at']=$value->created_at;
            $data[$key]['updated_at']=$value->updated_at;            
        }   

        return generateJson($data);                                  
    }

    function postGejala(Request $request)
    { 
        
        $dataGejala = [
            'nama_gejala' => $request->nama_gejala,            
            'created_by' => $request->created_by,
            'updated_by' => $request->updated_by,
        ];

        $query = Gejala::updateOrCreate(['id'=>$request->id], $dataGejala); 

        if ($query == true) {
            $data['code']="100";
            $data['message']="Data gejala sukses!";
        } else {
            $data['code']="404";
            $data['message']="Data gejala gagal!";
        }         
            
        return generateJson($data);
    }


    function postDGejala(Request $request)
    {
    
        $query = DB::table('m_gejala')
            ->where('id', $request['id'])->delete();  

        if ($query == true) {
            $data['code']="100";
            $data['message']="Sukses hapus data gejala!";
        } else {
            $data['code']="404";
            $data['message']="Gagal hapus data gejala!";
        }        

        return generateJson($data);
    }
}

