<?php

namespace App\Http\Controllers\master;

Use DB;
use Redirect;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Validator;

class historyController extends Controller
{
    function getHistory($id)
    {

        if($id == 'all'){                    

            $query = DB::table("history as h")
                ->selectRaw('h.*, p.nama as nama_p')
                ->leftJoin('pasien as p', 'h.id_pasien', '=', 'p.id')
                ->orderBy('nama_p', 'ASC')
                ->get();              
            
            $data = array();

            foreach ($query as $key => $value) {
                $data[$key]['id']=$value->id;
                $data[$key]['id_pasien']=$value->id_pasien;
                $data[$key]['nama_p']=$value->nama_p;
                $data[$key]['created_at']=$value->created_at;
            }   

            return response()->json($data);

        } else {

            $query = DB::table("history")       
                ->where('id', $id)                         
                ->get();                        

            return response()->json($query);

        }                
    }

    function postHistory(Request $request)
    {                

        $insert = array(         
            'id_pasien' => $request->id_pasien, 
            'created_at' => date("Y-m-d H:i:s")
        );                    

        $query = DB::table('history')->insert($insert); 

        if ( $query == true ){
            $data['code']="100";
            $data['message']="Sukses tambah data history!";
        } else {
            $data['code']="404";
            $data['message']="Gagal tambah data history!";
        }                

        return response()->json($data);
    }      

    function postDHistory(Request $request)
    {
    
        $query = DB::table('history')
            ->where('id', $request['id'])->delete();  

        if ( $query == true ) {
            $data['code']="100";
            $data['message']="Sukses hapus data history!";
        } else {
            $data['code']="404";
            $data['message']="Gagal hapus data history!";
        }        

        return response()->json($data);
    }
}

