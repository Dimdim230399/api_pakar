<?php

namespace App\Http\Controllers\master;
Use DB;
use Redirect;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Validator;
use App\Penyakit;


class penyakitController extends Controller
{
    function getPenyakit()
    {
        $query = DB::table("m_penyakit")                
        ->orderBy('nama_penyakit', 'ASC')
        ->get();
            
        $data = array();

        foreach ($query as $key => $value) {
            $data[$key]['id']=$value->id;
            $data[$key]['nama_penyakit']=$value->nama_penyakit;
            $data[$key]['created_by']=$value->created_by;
            $data[$key]['updated_by']=$value->updated_by;
            $data[$key]['created_at']=$value->created_at;
            $data[$key]['updated_at']=$value->updated_at;                
        }     
        
        return generateJson($data);
        
    }

    function postPenyakit(Request $request)
    {
        $dataPenyakit = [
            'nama_penyakit' => $request->nama_penyakit,                        
            'created_by' => $request->created_by,
            'updated_by' => $request->updated_by,
        ];

        $query = Penyakit::updateOrCreate(['id'=>$request->id], $dataPenyakit ); 
        
        if ($query == true) {
            $data['code']="100";
            $data['message']="Data penyakit sukses!";
        } else {
            $data['code']="404";
            $data['message']="Data penyakit gagal!";
        }  
            
        return generateJson($data);
    }     

    function postDPenyakit(Request $request)
    {
    
        $query = DB::table('m_penyakit')
            ->where('id', $request['id'])->delete();  

        if ( $query == true ) {
            $data['code']="100";
            $data['message']="Sukses hapus data penyakit!";
        } else {
            $data['code']="404";
            $data['message']="Gagal hapus data penyakit!";
        }        

        return generateJson($data);
    }
}

